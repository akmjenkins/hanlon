;(function(context) {

	var 
		tim,
		MVVM;
	
	if(context) {
		tim = context.tim;
	} else {
		tim = require('./tim.microtemplate.js');
	}
	
	
	MVVM = function(model,tmpl,$el) {
		var self = this;
		
		this.$el = $el;
		this.sorters = {};
		this.filters = {};
		this.originalModel = model;
		this.model = model;
		this.tmpl = tmpl;
		
	};
	
	MVVM.prototype.setTemplate = function(tmpl) {
		this.tmpl = tmpl;
	};
	
	MVVM.prototype.setModel = function(model) {
		this.originalModel = model;
		this.model = model;
		this.model && this.tmpl && this.render();
	};
	
	MVVM.prototype.setElement = function($el) {
		this.$el = $el;
		this.model && this.tmpl && this.render();
	};
	
	MVVM.prototype.add = function(modelObject) {
		this.originalModel.push(modelObject);
		this.model && this.tmpl && this.render();
	};
	
	MVVM.prototype.get = function(modelObjectIdOrRunnable) {
		var modelObject;
		
		if(typeof modelObjectIdOrRunnable === 'undefined') {
			return this.originalModel;
		}
		
		$.each(this.originalModel,function(i,o) {
			if(typeof modelObjectIdOrRunnable === 'function') {
				if(modelObjectIdOrRunnable.apply(o)) {
					modelObject = o;
					return false;
				}
				return true;
			}
			
			if(o.id == modelObjectIdOrRunnable) {
				modelObject = o;
				return false;
			}
		});
		
		return modelObject;
	};
	
	MVVM.prototype.getIndexOf = function(modelObjectIdOrRunnable) {
		var index;
		
		$.each(this.originalModel,function(i,o) {
			if(typeof modelObjectIdOrRunnable === 'function') {
				if(modelObjectIdOrRunnable.apply(o)) {
					index = i;
					return false;
				}
				return true;
			}
			
			if(o.id == modelObjectIdOrRunnable) {
				index = i;
				return false;
			}
		});
		
		return index;
	}
	
	MVVM.prototype.update = function(modelObjectIdOrRunnable,newModelObjectOrRunnable) {
		var index = this.getIndexOf(modelObjectIdOrRunnable);
		
		if(typeof index !== 'undefined') {
			if(typeof newModelObjectOrRunnable === 'function') {
				newModelObjectOrRunnable.apply(this.originalModel[index]);
			} else {
				this.originalModel.splice(index,1,newModelObjectOrRunnable);
			}
			
			this.tmpl && this.render();
		}
		
	};
	
	MVVM.prototype.modifyAll = function(runnable) {
		$.each(this.originalModel,function(i,o) {
			runnable.apply(o);
		});
	};
	
	MVVM.prototype.remove = function(modelObjectIdOrRunnable) {
		var index = this.getIndexOf(modelObjectIdOrRunnable);
		typeof index !== 'undefined' && this.originalModel.splice(index,1);
		this.tmpl && this.render();
	};
	
	MVVM.prototype.getTotalCount = function() {
		return this.originalModel.length;
	};
	
	MVVM.prototype.getCurrentCount = function() {
		return this.model.length;
	};
	
	MVVM.prototype.addSorter = function(name,runnable,order) {
		this.sorters[name] = {runnable:runnable,order:order};
		return this.sorters[name];
	};
	
	MVVM.prototype.removeSorter = function(name) {
		delete this.sorters[name];
		this.runSorters();
	};
	
	MVVM.prototype.runSorters = function() {
		var self = this;
		this.model.sort(function(a,b) {
			var returnValue = 0;
			$.each(self.sorters,function(name,o) {
				returnValue = o.runnable.apply(window,[a,b,o.order]);
				
				//if a sort can be determined using this sorter (1 or -1)
				//then we do not need to continue to loop through self.sorters
				if(returnValue !== 0) {
					return false;
				}
			});
			
			return returnValue;
		});
	};
	
	MVVM.prototype.clearSorters = function() {
		this.sorters = {};
		this.runSorters();
	};
	
	MVVM.prototype.addFilter = function(name,runnable) {
		this.filters[name] = runnable;
	};
	
	MVVM.prototype.removeFilter = function(name) {
		delete this.filters[name];
		this.runFilters();
	};
	
	MVVM.prototype.runFilters = function() {
		var 
			self = this,
			newModel = [],
			excluded = [];
		
		//loop over each advertiser
		$.each(this.originalModel,function(indexOfModelObject,modelObject) {
			var shouldAdd = true; //set to false, if filter returns false (below)
			
			var indexInExcluded = excluded.indexOf(modelObject);
			
			//if this advertiser already exists in the removed array, we can skip the running of the filters
			if(indexInExcluded !== -1) { return; }
			
			//loop over each filter and perform it's test - runnable.apply(advertiser)
			$.each(self.filters,function(name,runnable) {
				
				var indexInModel;
				
				//perform the test
				if(!runnable.apply(modelObject,[indexOfModelObject])) {
					shouldAdd = false;
					
					//failed the filter test, remove it from the model if it already exists
					excluded.push(modelObject);
					indexInModel = newModel.indexOf(modelObject);
					indexInModel === -1 || newModel.splice(indexInModel,1);
					
				}
			});
		
			//if the filter did not fail (i.e. shouldAdd = true), then add this advertiser to the model
			shouldAdd && newModel.push(modelObject);
			
		});
		
		this.model = newModel;
	};
	
	MVVM.prototype.clearFilters = function() {
		this.filters = {};
		this.runFilters();
	};
	
	MVVM.prototype.render = function(returnHTML) {
		var self = this;
		
		this.runFilters();
		this.runSorters();
		
		var html = '';
		$.each(this.model,function(i,modelObject) {
			html += tim(self.tmpl,modelObject)
		});
		
		if(returnHTML) {
			return html;
		}
		
		this.$el.html(html);
		this.$el.trigger('mvvm.rendered',[this]);
	};

	
	
	//CommonJS
	if(typeof module !== 'undefined' && module.exports) {
		module.exports = MVVM;
	//CodeKit
	} else if(context) {
		context.MVVM = MVVM;
	}	

}(typeof ns !== 'undefined' ? window[ns] : undefined));