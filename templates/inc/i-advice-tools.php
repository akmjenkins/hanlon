<div class="split-block full advice-tools with-border">

	<div class="split-block-item">
		<div class="split-block-content">
		
			<div class="advice-block">
			
				<h2>Advice</h2>
				
				<form action="/" class="single-form">
					<div class="fieldset">
					
						<input type="text" name="question" placeholder="Ask a Question">
						<button class="t-fa-abs fa-angle-right">Search</button>
					
					</div><!-- .fieldset -->
				</form><!-- .single-form -->
				
				<h4>Recent Questions</h4>
				
				<a href="#link" class="inline">What are requirements to buy your first home?</a> <br />
				<a href="#link" class="inline">How long does it take to buy a home?</a> <br />
				
				<br />
				
				<a href="#" class="button">Contact Realtor</a>
			
			</div><!-- .advice-block -->

		</div><!-- .split-block-content -->
	</div><!-- .split-block-item -->

	<div class="split-block-item">
		<div class="split-block-content">

			<div class="tools-block">
			
				<h2>Tools</h2>	
				
				<div class="grid nopad">
				
					<div class="col col-2 sm-col-1">
						<div class="item pad-20">
							<i class="t-fa fa-home ico"></i>
							<h5>How much is my home worth?</h5>
							<p>
								Enter your address information email address and we will provide you with a 
								personalized estimate of your home emailed to you. If you like we can have a 
								Realtor contact you at a time that is convient for you.
							</p>
							
							<a href="#" class="button">Learn More</a>
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col col-2 sm-col-1">
						<div class="item pad-20">
							<i class="t-fa fa-bar-chart ico"></i>
							<h5>How much can I afford?</h5>
							<p>
								Use the home affordability calculator to determine how much you can afford. 
								Enter details about your income, monthly debts and down payment to find 
								a home within your budget.
							</p>
							
							<a href="#" class="button">Learn More</a>
						</div><!-- .item -->
					</div><!-- .col -->
					
				</div><!-- .grid -->
				
			</div><!-- .tools-block -->
		
			
			
			
		
		</div><!-- .split-block-content -->
	</div><!-- .split-block-item -->	

</div><!-- .split-block -->