<div class="featured-properties">

	<div class="section-title">
		<h2>Hanlon Featured Properties</h2>
	</div><!-- .section-title -->

	
	<div class="grid eqh fp-grid">

		<div class="col">
			<div class="item fp-item">
				
				<div class="fp-img lazybg" data-src="../assets/dist/images/temp/home-1.jpg"></div>
				<span class="fp-price">$429,000</span>

				<div class="fp-address">
					<span class="fp-address-street">76 Yellowwod Drive</span>
					<span class="fp-address-city">St. John's, NL</span>
				</div><!-- .fp-address -->

				<div class="fp-specs">
					<div class="fp-spec">
						2 <span>beds</span>
					</div><!-- .fp-spec -->
					<div class="fp-spec">
						3 <span>baths</span>
					</div><!-- .fp-spec -->
					<div class="fp-spec">
						1543 <span>Sq. Ft.</span>
					</div><!-- .fp-spec -->
				</div><!-- .fp-specs -->

				<span class="fp-brokerage">Listed by COLDWELL BANKER PROCO</span>

				<div class="fp-buttons">
					<a href="#" class="button primary fill toggle-favorite" data-listing-id="00001">Save</a>
					<a href="#" class="button primary fill">View Details</a>
				</div><!-- .fp-buttons -->

			</div><!-- .item -->
		</div><!-- .col -->

		<div class="col">
			<div class="item fp-item">
				
				<div class="fp-img lazybg" data-src="../assets/dist/images/temp/home-1.jpg"></div>
				<span class="fp-price">$429,000</span>

				<div class="fp-address">
					<span class="fp-address-street">76 Yellowwod Drive</span>
					<span class="fp-address-city">St. John's, NL</span>
				</div><!-- .fp-address -->

				<div class="fp-specs">
					<div class="fp-spec">
						2 <span>beds</span>
					</div><!-- .fp-spec -->
					<div class="fp-spec">
						3 <span>baths</span>
					</div><!-- .fp-spec -->
					<div class="fp-spec">
						1543 <span>Sq. Ft.</span>
					</div><!-- .fp-spec -->
				</div><!-- .fp-specs -->

				<span class="fp-brokerage">Listed by COLDWELL BANKER PROCO</span>

				<div class="fp-buttons">
					<a href="#" class="button primary fill toggle-favorite" data-listing-id="00001">Save</a>
					<a href="#" class="button primary fill">View Details</a>
				</div><!-- .fp-buttons -->

			</div><!-- .item -->
		</div><!-- .col -->

		<div class="col">
			<div class="item fp-item">
				
				<div class="fp-img lazybg" data-src="../assets/dist/images/temp/home-1.jpg"></div>
				<span class="fp-price">$429,000</span>

				<div class="fp-address">
					<span class="fp-address-street">76 Yellowwod Drive</span>
					<span class="fp-address-city">St. John's, NL</span>
				</div><!-- .fp-address -->

				<div class="fp-specs">
					<div class="fp-spec">
						2 <span>beds</span>
					</div><!-- .fp-spec -->
					<div class="fp-spec">
						3 <span>baths</span>
					</div><!-- .fp-spec -->
					<div class="fp-spec">
						1543 <span>Sq. Ft.</span>
					</div><!-- .fp-spec -->
				</div><!-- .fp-specs -->

				<span class="fp-brokerage">Listed by COLDWELL BANKER PROCO</span>

				<div class="fp-buttons">
					<a href="#" class="button primary fill toggle-favorite" data-listing-id="00001">Save</a>
					<a href="#" class="button primary fill">View Details</a>
				</div><!-- .fp-buttons -->

			</div><!-- .item -->
		</div><!-- .col -->

		<div class="col">
			<div class="item fp-item">
				
				<div class="fp-img lazybg" data-src="../assets/dist/images/temp/home-1.jpg"></div>
				<span class="fp-price">$429,000</span>

				<div class="fp-address">
					<span class="fp-address-street">76 Yellowwod Drive</span>
					<span class="fp-address-city">St. John's, NL</span>
				</div><!-- .fp-address -->

				<div class="fp-specs">
					<div class="fp-spec">
						2 <span>beds</span>
					</div><!-- .fp-spec -->
					<div class="fp-spec">
						3 <span>baths</span>
					</div><!-- .fp-spec -->
					<div class="fp-spec">
						1543 <span>Sq. Ft.</span>
					</div><!-- .fp-spec -->
				</div><!-- .fp-specs -->

				<span class="fp-brokerage">Listed by COLDWELL BANKER PROCO</span>

				<div class="fp-buttons">

					<!-- give this element a class of is-favourite, if this property is a favourite property of the currently logged in user -->
					<a href="#" class="button primary fill toggle-favorite is-favorite" data-listing-id="00001">Saved</a>
					<a href="#" class="button primary fill">View Details</a>
					
				</div><!-- .fp-buttons -->

			</div><!-- .item -->
		</div><!-- .col -->

	</div><!-- .grid -->
		
	<div class="center">
		<a href="#" class="button">View All</a>
	</div><!-- .center -->

</div><!-- .featured-properties -->