			<footer>

				<div class="sw footer-blocks">

					<div class="footer-block">
						<ul class="footer-nav">
							<li><a href="#">Buy</a></li>
							<li><a href="#">Sell</a></li>
							<li><a href="#">Mortgages</a></li>
							<li><a href="#">Realtors&reg;</a></li>
							<li><a href="#">Advice</a></li>
						</ul>
					</div><!-- .footer-nav -->

					<div class="footer-meta-nav footer-block">
						<ul class="footer-nav">
							<li><a href="#">About</a></li>
							<li><a href="#">Contact</a></li>
						</ul>

						<?php include('i-social.php'); ?>
					</div><!-- .footer-meta-nav -->

					<div class="footer-address footer-block">
						<div class="lazybg with-img footer-address-logo">
							<img src="../assets/dist/images/hanlon-logo-white.svg" alt="Hanlon Realty Logo">
						</div><!-- .lazybg -->

						<address>
							Hanlon Realty Newfoundland and Labrador <br>
							95 Bonaventure Avenue, St. John's, NL A1B 2X5
						</address>

						<span class="footer-phone">Tel: 709 738 6200</span>
						<span class="footer-phone">Fax: 709 738 6201</span>
					</div><!-- .footer-address -->

				</div><!-- .sw -->

				<div class="copyright">
					<div class="sw">
						<ul>
							<li>Copyright &copy; <?php echo date('Y'); ?> <a href="/">Hanlon Realty</a> All Rights Reserved.</li>
							<li><a href="#">Sitemap</a></li>
							<li><a href="#">Legal</a></li>
						</ul>
						
						<a href="http://jac.co" rel="external" title="JAC. We Create." id="jac"><img src="../assets/dist/images/jac-logo.svg" alt="JAC Logo."></a>
					</div><!-- .sw -->
				</div><!-- .copyright -->
				
			</footer><!-- footer -->

		</div><!-- .page-wrapper -->

		<script>
			var templateJS = {
				templateURL: 'http://dev.me/hanlon',
				CACHE_BUSTER: '<?php echo time(); ?>'	/* cache buster - set this to some unique string whenever an update is performed on js/css files, or when an admin is logged in */
			};
		</script>

		<script src="../assets/dist/js/main.gulp.js"></script>
	</body>
</html>