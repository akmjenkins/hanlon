<div class="split-block full advice-tools with-border">

	<div class="split-block-item">
		<div class="split-block-content">
		
			<div class="advice-block">
			
				<h2>Advice</h2>
				
				<form action="/" class="body-form">
					<div class="fieldset">
						<input type="text" name="question" placeholder="Ask a Question">
					</div><!-- .fieldset -->
					<button class="button">Contact Realtor</button>
				</form><!-- .single-form -->
				
				
			
			</div><!-- .advice-block -->

		</div><!-- .split-block-content -->
	</div><!-- .split-block-item -->

	<div class="split-block-item">
		<div class="split-block-content">

			<div class="tools-block">
			
				<h2>Tools</h2>	
				
				<div class="grid">
				
					<div class="col col-3 sm-col-1">
						<div class="item">
							<i class="t-fa fa-star-o ico"></i>
							<h5>How much is my home worth?</h5>
							<a href="#" class="button">Learn More</a>
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col col-3 sm-col-1">
						<div class="item">
							<i class="t-fa fa-home ico"></i>
							<h5>What's my monthly payment?</h5>
							<a href="#" class="button">Learn More</a>
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col col-3 sm-col-1">
						<div class="item">
							<i class="t-fa fa-bar-chart ico"></i>
							<h5>How much can I afford?</h5>							
							<a href="#" class="button">Learn More</a>
						</div><!-- .item -->
					</div><!-- .col -->
					
				</div><!-- .grid -->
				
			</div><!-- .tools-block -->
		
			
			
			
		
		</div><!-- .split-block-content -->
	</div><!-- .split-block-item -->	

</div><!-- .split-block -->