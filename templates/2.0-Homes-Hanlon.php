<?php $bodyclass = 'map-page'; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<div class="d-bg secondary-bg property-search">
	
		<button class="property-search-handle t-fa-abs fa-search">Search</button>
		<button class="property-search-close t-fa-abs fa-close">Close</button>
	
		<div class="sw">
		
			<form action="" id="property-search-form">
		
				<label>
					<small>Minimum Price</small>
					<div class="selector with-arrow">
						<select name="min_price">
							<option value="-1">Any</option>
							<option value="0">$0</option>
							<option value="25000">$25,000</option>
							<option value="50000">$50,000</option>
							<option value="75000">$75,000</option>
							<option value="100000">$100,000</option>
							<option value="150000">$150,000</option>
							<option value="200000">$200,000</option>
							<option value="250000">$250,000</option>
							<option value="30000">$300,000</option>
							<option value="350000">$350,000</option>
							<option value="400000">$400,000</option>
							<option value="450000">$450,000</option>
							<option value="500000">$500,000</option>
							<option value="550000">$550,000</option>
							<option value="600000">$600,000</option>
							<option value="650000">$650,000</option>
							<option value="700000">$700,000</option>
							<option value="800000">$800,000</option>
							<option value="900000">$900,000</option>
							<option value="1000000">$1,000,000</option>
							<option value="1250000">$1,250,000</option>
							<option value="1500000">$1,500,000</option>
							<option value="1750000">$1,750,000</option>
							<option value="2000000">$2,000,000</option>
							<option value="3000000">$3,000,000</option>
							<option value="4000000">$4,000,000</option>
							<option value="5000000">$5,000,000+</option>
						</select>
						<span class="value"></span>
					</div><!-- .selector -->
				</label>
				
				<label>
					<small>Maximum Price</small>
					<div class="selector with-arrow">
						<select name="max_price">
							<option value="-1">Any</option>
							<option value="0">$0</option>
							<option value="25000">$25,000</option>
							<option value="50000">$50,000</option>
							<option value="75000">$75,000</option>
							<option value="100000">$100,000</option>
							<option value="150000">$150,000</option>
							<option value="200000">$200,000</option>
							<option value="250000">$250,000</option>
							<option value="30000">$300,000</option>
							<option value="350000">$350,000</option>
							<option value="400000">$400,000</option>
							<option value="450000">$450,000</option>
							<option value="500000">$500,000</option>
							<option value="550000">$550,000</option>
							<option value="600000">$600,000</option>
							<option value="650000">$650,000</option>
							<option value="700000">$700,000</option>
							<option value="800000">$800,000</option>
							<option value="900000">$900,000</option>
							<option value="1000000">$1,000,000</option>
							<option value="1250000">$1,250,000</option>
							<option value="1500000">$1,500,000</option>
							<option value="1750000">$1,750,000</option>
							<option value="2000000">$2,000,000</option>
							<option value="3000000">$3,000,000</option>
							<option value="4000000">$4,000,000</option>
							<option value="5000000">$5,000,000+</option>
						</select>
						<span class="value"></span>
					</div><!-- .selector -->
				</label>
				
				<label>
					<small>Beds</small>
					<div class="selector with-arrow">
						<select name="beds">
							<option value="-1">Any</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6+</option>
						</select>
						<span class="value"></span>
					</div><!-- .selector -->
				</label>
				
				<label>
					<small>Baths</small>
					<div class="selector with-arrow">
						<select name="baths">
							<option value="-1">Any</option>
							<option value="1">1</option>
							<option value="1.5">1.5</option>
							<option value="2">2</option>
							<option value="2.5">2.5</option>
							<option value="3">3</option>
							<option value="4">4+</option>
						</select>
						<span class="value"></span>
					</div><!-- .selector -->
				</label>
				
				<label>
					<small>Baths</small>
					<div class="selector with-arrow">
						<select name="property_type">
							<option value="0">Any Type</option>
							<option value="1">Single Family</option>
							<option value="2">Duplex</option>
							<option value="3">Multiplex</option>
							<option value="4">Commercial</option>
							<option value="5">Condominium</option>
							<option value="6">Land &amp; Acreage</option>
							<option value="7">Other</option>
							<option value="8">Cottage &amp; Recreational</option>
							<option value="9">Agricultural</option>
							<option value="10">Mobile / Mini</option>
						</select>
						<span class="value"></span>
					</div><!-- .selector -->
				</label>
			
				<button class="button">Search</button>
			
			</form>
		
		</div><!-- .sw -->
	</div><!-- .property-search -->

	<div class="map-wrap hanlon-map">

		<div class="sw map-buttons">
			<button class="button fill t-fa fa-picture-o list-button">Properties</button>
			<button class="button fill t-fa fa-map-marker map-button">Map</button>
			<span class="t-fa-abs fa-spin fa-circle-o-notch map-loading-indicator">Searching...</span>
		</div><!-- .sw -->

		<div class="map-canvas-wrap">
		
			<div class="map-canvas">
			</div><!-- .map-canvas -->
			
			<div class="map-zoom-controls">
				<button class="zoom-in" title="Zoom In">&plus;</button>
				<button class="zoom-out" title="Zoom Out">&minus;</button>
			</div>
			
			<div class="responsive-info-window">&nbsp;</div>
		</div><!-- .map-canvas-wrap -->
		
		<div class="list-canvas-wrap">
		
			<div class="sw">		
				<div class="grid eqh fp-grid list-canvas">
					<!-- MVVM content goes here -->
				</div><!-- .grid -->
				
				<div class="center">
					<div class="button show-more-properties secondary">Show More Properties</div>
				</div><!-- .center -->
				
			</div><!-- .sw -->
			
		</div><!-- .list-canvas -->
		
	</div><!-- .map-wrap -->

	<section class="d-bg primary-bg nopad">
		<div class="sw">
			<?php include('inc/i-advice-tools-inside.php'); ?>
		</div><!-- .sw -->
	</section><!-- .d-bg -->	
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>